<?php

// include_once APPPATH.'controllers/fb-callback.php';

defined('BASEPATH') or exit('No direct script access allowed');

class Inicio extends CI_Controller
{
    public function Index()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('LoginModel', 'model');

        $data['loginUrl'] = $this->model->GetLogin();

        $this->load->view('Login/index', $data);

        $this->load->view('common/footer');
    }


    public function Sobre()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->view('Login/sobre');

        $this->load->view('common/footer');
    }

    public function Dados()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->view('Login/dados');

        $this->load->view('common/footer');
    }
}
