<?php

//Incluiu assim direto pois estão no mesmo diretório, senão teria que colocar o caminho
include_once 'Pessoa.php';

//leia extends como : aluno é pessoa

class Aluno extends Pessoa{
    private $turma;


    function __construct($nome,$idade,$turma){
        parent::__construct($nome,$idade);
        $this->turma=$turma;
    }

    public function setTurma($turma){
        $this->turma = $turma;
    }
        
    public function getTurma(){
        return $this->turma;
    }



}
