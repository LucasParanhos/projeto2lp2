<?php

class Pessoa 
{   //atributos
    private $nome;
    private $idade;
    private $pressao;

    //contrutor
    function __construct($nome,$idade){
        $this->nome = $nome;
        $this->idade = $idade;

    }

    //métodos acessores (get/set  ou getters/setters)

    public function getNome(){
        return $this->nome;
    }

    public function getIdade(){
        return $this->idade;
    }

    public function setIdade($idade){
       $this->idade=$idade;
    }


    public function getPressao(){
        $s = rand(5,20);
        $d = rand(3,18);
        
        if ($s < $d){
            $aux = $s;
            $s = $d;
            $d = $aux;
        }

        return "$s x $d";
     }


     public function andar(){
        $x = rand(-30,30);
        $y = rand(-30,30);
        return "$x,$y";
     }


}

class Automovel 
{ 
    private $marca;
    private $modelo;

    /**
     * Recebe marca e modelo do Automóvel
     * @param marca string contendo marca do veículo
     * @param modelo string contendo modelo do veículo
     */
    function __construct($marca,$modelo){
        $this->marca = $marca;
        $this->modelo = $modelo;
    }

    /**
     * Retorna o marca do automóvel
     */
    public function getMarca(){
        return $this->marca;
    }
    
    /**
     * Altera marca do Automóvel
     * @param marca string contendo a nova marca do automóvel
     */
    public function setMarca($marca){
       $this->marca = $marca;
    }

    /**
     * Retorna o modelo do automóvel
     */
    public function getModelo(){
        return $this->modelo;
    }
    
    /**
     * Altera o modelo do Automóvel
     * @param modelo string contendo a nova marca do automóvel
     */
    public function setModelo($modelo){
       $this->modelo = $modelo;
    }

    /** 
     * move aleatóriamente o objeto em um plano cartesiano
    */
    public function mover(){
       $x = rand(-10,10);
       $y = rand(-10,10);
       return "$x,$y";
    }

    /** 
     * liga o motor do automóvel
    */
    public function ligarMotor(){
       return "Seu motor está ligado";
    }

}

class Motocicleta extends Automovel{
    private $cor;
    private $carta_motorista;


    /**
     * Recebe marca e modelo do Automóvel
     * @param marca string contendo marca do veículo
     * @param modelo string contendo modelo do veículo
     * @param cor string recebe hash da cor da Motocicleta
     * @param modelo char recebe tipo da carta de motorista
     */
    function __construct($marca,$modelo,$cor,$carta_motorista){
        parent::__construct($marca,$modelo);
        $this->cor = $cor;
        $this->carta_motorista = $carta_motorista;

    }
 
     /**
      * Retorna a cor da motocicleta
      */
     public function getCor(){
         return $this->cor;
     }

     /**
      * Retorna a carta da motocicleta
      */
     public function getCarta_motorista(){
         return $this->carta_motorista;
     }

     /** 
      * empina motocicleta
     */
     public function enpinar(){
        return "Sua motocicleta se encontra empinada agora";
     }
     /** 
      * trabalha com a motocicleta
     */
     public function trabalhar(){
        return "Você usa a motocicleta para trabalho";
     }
}

class Aviao extends Automovel{
    private $compania;
    private $capacidade;


    /**
     * Recebe marca e modelo do Automóvel
     * @param marca string contendo marca do veículo
     * @param modelo string contendo modelo do veículo
     * @param compania string recebe nome da compania
     * @param capacidade int recebe o numero maximo de passageiros e funcionarios no avião
     */
    function __construct($marca,$modelo,$compania,$capacidade){
        parent::__construct($marca,$modelo);
        $this->compania = $compania;
        $this->capacidade = $capacidade;

    }
 
     /**
      * Retorna a compania áerea do avião
      */
     public function getCompania(){
         return $this->compania;
     }

     /**
      * Retorna a capacidade da aeronave
      */
     public function getCapacidade(){
         return $this->capacidade;
     }

    /** 
     * move aleatóriamente o avião em uma matriz tridimencional
    */
    public function mover(){
        $x = rand(-999,999);
        $y = rand(-999,999);        
        $z = rand(0,999);
        return "$x,$y,$z";
     }
     /** 
      * Você viaja de avião
     */
     public function Viajar(){
        return "Você usa o avião para ir viajar";
     }
}


class Triciclo 
{ 
    private $bancos;
    private $preco;

    /**
     * Recebe marca e modelo do Automóvel
     * @param bancos int contendo número de bancos do triciclo
     * @param modelo float contendo valor do triciclo
     */
    function __construct($bancos,$preco){
        $this->bancos = $bancos;
        $this->preco = $preco;
    }

    /**
     * @return int numero de bancos do triciclo
     */
    public function getBancos(){
        return $this->bancos;
    }
    
    /**
     * @return float preço do triciclo
     */
    public function getPreco(){
        return $this->preco;
    }    

    /** 
     * move aleatóriamente o objeto em um plano cartesiano
     * @return string posição nova do triciclo
    */
    public function mover(){
       $x = rand(-5,5);
       $y = rand(-5,5);
       return "$x,$y";
    }

    /** 
     * liga o motor do automóvel
    */
    public function PedalarMotor(){
       return "Você está pedalando para longe!";
    }

}