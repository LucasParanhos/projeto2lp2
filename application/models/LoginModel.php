<?php

defined('BASEPATH') or exit('No direct script access allowed');

class LoginModel extends CI_Model
{
    function GetLogin()
    {
        require_once 'lib/Facebook/autoload.php';

        session_start();
        $fb = new Facebook\Facebook([
            'app_id' => '320999951904350',
            'app_secret' => '375d6e4037dbc9327bad4da6fea80045',
            'default_graph_version' => 'v3.2',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('localhost/fb-callback.php', $permissions);

        return $loginUrl;
    }
}
