<div class="col-md-12">
    <h3>Sobre a API</h3>

    <ol class="text-dark">
        <li>Quais são as principais características da API e suas finalidades?
            <ul>
                <li>Login atravez do google</li>
                <li>Login atravez do facebook</li>
                <li>Sua finalidade está em facilitar a vida do usuario, não exigindo que ele decore dezenas de senhas para cada site</li>
            </ul>
        </li>
        <li>O quê é possível criar com o uso desta API?
            <ul>
                <li>telas de login e cadastro de diversos tipos de sistema</li>
            </ul>
        </li>
        <li>Quais são as restrições para o uso da API? Tem custo? Tem boa documentação? E outras...
            <ul>
                <li>A API é gratuita e tem documentação ampla, o unico requisito é possuir um https</li>
            </ul>
        </li>
        <li>Quais foram os passos necessários para a implementação da sua aplicação com o uso desta API?
            <ul>
                <li>Estudar a documentação</li>
                <li>Criação de um app pelo facebook developer</li>
                <li>Implementar no sistema</li>
                <li>Buscar dados do servidor</li>
            </ul>
        </li>
    </ol>
</div>