

<div id="login" class="container" style="margin-top: 100px">
    <div class="row justify-content-center">
        <div class="col-md-6 col-offset-3" align="center">
            
            <div id="divLogin">
                <h1>Login do facebook</h1>
                <h3>Para prosseguir clique no link abaixo</h3>
                <!-- <div class="fb-login-button" onclick="faceLogin()" data-width="" data-size="large" data-button-type="continue_with" data-auto-logout-link="false" data-use-continue-as="false"></div> -->
                <button value="Entrar com o Facebook" onclick="faceLogin()" class="btn btn-default">Entrar com o Facebook</button>

            </div>
        </div>
    </div>
</div>

<div id="dados" class="text-center col-md-12 text-middle mt-50" style="display: none;">

    <h2 class="mb-30">Dados obtidos através do facebook</h2>
    <h3 id="nome"></h3>
    <img id="foto" class="height-200 rounded" />
    <h4 id="email"></h4>
</div>

