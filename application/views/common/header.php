<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Projeto II LP II</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/mdb.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/style.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


  <script>

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

    function faceLogout() {
      FB.logout(function(response) {        
          $('#login').show();
          $('#dados').hide();
      });
    }

    function faceLogin() {

      FB.login(function(response) {
        if (response.status === 'connected') {
          $('#login').hide();
          $('#dados').show();
          getData();

        } else {
          alert('Não foi possivel fazer login, por favor tente novamente');
        }
      }, {
        scope: 'public_profile,email'
      });

      function getData(){
      FB.api('/me?fields=name,email,picture', function(response) {
        $('#nome').html(response.name);
        $('#email').html(response.email);
        $('#foto').attr('src', response.picture.data.url);
      });
      }
    };
  </script>
</head>

<body>