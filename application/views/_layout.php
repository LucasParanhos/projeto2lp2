<html>
<head>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="//<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="//<?php echo base_url() ?>assets/css/mdb.css" rel="stylesheet">
    
    <link href="//<?php echo base_url() ?>css/Layout.css" rel="stylesheet">



    <!-- JQuery -->
    <script type="text/javascript" src="//<?php echo base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="//<?php echo base_url() ?>assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="//<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="//<?php echo base_url() ?>assets/js/mdb.min.js"></script>
</head>

<body>
<nav class="navbar navbar-dark bg-danger navbar-expand-lg">
  <a class="navbar-brand" href="#">Exemplo</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="//<?php echo base_url() ?>">Sobre <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="//<?php echo base_url() ?>index.php/Welcome/produtos">Produtos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="//<?php echo base_url() ?>index.php/Welcome/contato">Contato</a>
      </li>
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li> -->
  </div>
</nav>


    <div class="content pr-5 pl-5 pt-2 pb-2">
         <?= $pagina ?>
    </div>

    
<footer class="page-footer font-small bg-danger">
    <div class="text-center  text-white">
      © 2019 Copyright: Lucas Sanches Paranhos GU3002501
    </div>
</footer>

</body>
</html>